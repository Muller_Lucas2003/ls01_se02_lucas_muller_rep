public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.printf("%-5s","0!");
		System.out.print(" = ");
		System.out.printf("%-19s","");
		System.out.print(" = ");
		System.out.printf("%4d \n",1);
		
		System.out.printf("%-5s","1!");
		System.out.print(" = ");
		System.out.printf("%-19s","1");
		System.out.print(" = ");
		System.out.printf("%4d \n",1);
		
		System.out.printf("%-5s","2!");
		System.out.print(" = ");
		System.out.printf("%-19s","1 * 2");
		System.out.print(" = ");
		System.out.printf("%4d \n",1*2);
		
		System.out.printf("%-5s","3!");
		System.out.print(" = ");
		System.out.printf("%-19s","1 * 2 * 3");
		System.out.print(" = ");
		System.out.printf("%4d \n",1*2*3);
		
		System.out.printf("%-5s","4!");
		System.out.print(" = ");
		System.out.printf("%-19s","1 * 2 * 3 * 4");
		System.out.print(" = ");
		System.out.printf("%4d \n",1*2*3*4);
		
		System.out.printf("%-5s","4!");
		System.out.print(" = ");
		System.out.printf("%-19s","1 * 2 * 3 * 4 * 5");
		System.out.print(" = ");
		System.out.printf("%4d \n",1*2*3*4*5);
	}

}
