import java.util.Scanner;

public class Aufgabe1z�hlen {

	public static void main(String[] args) {

		int x;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine nat�rliche Zahl ein: ");
		x = myScanner.nextInt();

		System.out.println("Count up:");
		countUp(x);

		System.out.println("\nCount down:");
		countDown(x);

	}

	public static void countUp(int pX) {
		int i = 1;
		while ( i <= pX ) {
			System.out.println(i);
			i++;
		}
	}

	public static void countDown(int pX) {
		int i = pX;
		while ( i > 0 ) {
			System.out.println(i);
			 i--;
		}
	}

}