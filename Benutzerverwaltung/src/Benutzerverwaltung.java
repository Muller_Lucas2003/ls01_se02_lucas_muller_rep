import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {

	public static void main(String[] args) {

		ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();
		int auswahl;

		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				benutzerAnzeigen(benutzerliste);
				break;
			case 2:
				benutzerErfassen(benutzerliste);
				break;
			case 3:
				// benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");
			}
		} while (true);

	}

	public static int menu() {

		int selection;
		Scanner input = new Scanner(System.in);

		/***************************************************/

		System.out.println("     Benutzerverwaltung     ");
		System.out.println("----------------------------\n");
		System.out.println("1 - Benutzer anzeigen");
		System.out.println("2 - Benutzer erfassen");
		System.out.println("3 - Benutzer löschen");
		System.out.println("4 - Ende");

		System.out.print("\nEingabe:  ");
		selection = input.nextInt();
		return selection;
	}

	public static void benutzerAnzeigen(ArrayList<Benutzer> benutzerliste) {
		System.out.println(benutzerliste);
	}

	public static void benutzerErfassen(ArrayList<Benutzer> benutzerliste) {
		Scanner input = new Scanner(System.in);
		System.out.println("Geben Sie bitte nacheinender folgende Benutzerdaten ein:");
		System.out.print("Benutzernummer: ");
		int bn = input.nextInt();
		System.out.println();
		System.out.print("Vorname: ");
		String vn = input.next();
		System.out.println();
		System.out.print("Nachname: ");
		String nn = input.next();
		System.out.println();
		System.out.print("Geburtsjahr: ");
		int gj = input.nextInt();
		System.out.println();
		Benutzer b1 = new Benutzer(bn, vn, nn, gj);
		benutzerliste.add(b1);
	}

}
